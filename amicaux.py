

def sommediv(n):
    s = 1
    i = 2
    while i ** 2 < n:
        if n % i == 0:
            s = s + i + n // i
        i = i + 1
    if i ** 2 == n:
        s = s + i
    return s

def amicaux(n, verbose=True):
    """ Affiche (si verbose = True) les couples de nombres amicaux
        dont le plus grand ne dépasse pas n
        Renvoie la liste avec les couples
    """
    res = []
    for a in range(n):
        b = sommediv(a)
        if a > b and sommediv(b) == a:
            if verbose:
                print(a, b)
            res.append((a, b))
    return res

assert sommediv(6) == 6
assert sommediv(28) == 28
assert sommediv(31) == 1
assert sommediv(220) == 284
assert amicaux(1500, False) == [(284, 220), (1210, 1184)]

print("Calcul pour 50000")
amicaux(50000)
