# Fichier README

`git` est un gestionnaire de versions collaboratif.

## Q-Suite

La Q-Suite est définie ainsi :
 
$$\left\{\begin{array}{l}
Q(1) = 1\\
Q(2) = 1\\
 Q(n) = Q(n - Q(n-1)) + Q(n - Q(n-2))), n > 2\\
\end{array}\right.$$

Exercice : Écrire un programme qui calcule le terme n

## Amicaux

Écrire une procédure qui affiche des coupes de nombres amicaux.
Deux nombres sont amicaux s'ils sont différents et si la somme des 
diviseurs stricts de l'un est égale à l'autre et inversement.  
Par exemple : 220 et 284 sont amicaux.



