"""
Calcule tous les termes de la suite de fibonacci de U_0 à U_n
"""

def fibo(n):
    if n < 0:
        return []
    lst = [0, 1]
    while len(lst) < n + 1:
        lst.append(lst[-1] + lst[-2])
    return lst[:n + 1]

print(fibo(20))
