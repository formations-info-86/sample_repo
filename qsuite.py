def qsuite_rec(n):
    """
    La Q-Suite est définie ainsi :
     Q(1) = 1
     Q(2) = 1
     Q(n) = Q(n - Q(n-1)) + Q(n - Q(n-2))), n > 2
    """

    # Implémentation récursive
    if n <= 0:
        return 0
    if n <= 2:
        return 1
    return qsuite_rec(n - qsuite_rec(n - 1)) + qsuite_rec(n - qsuite_rec(n - 2))

def qsuite_efficace(n):
    """
    La Q-Suite est définie ainsi :
     Q(1) = 1
     Q(2) = 1
     Q(n) = Q(n - Q(n-1)) + Q(n - Q(n-2))), n > 2
    """

    # Implémentation non récursive (programmation dynamique)
    if n <= 0:
        return 0
    suite = [0, 1, 1]
    k = 3
    # Pas terminé 
    while len(suite) < n + 1:
        suite.append(suite[k - suite[k - 1]] + suite[k - suite[k - 2]])
        k = k + 1
    return suite[n]

# Programme principal

qsuite = qsuite_efficace

assert qsuite(1) == 1
assert qsuite(2) == 1
assert qsuite(3) == 2
assert qsuite(6) == 4
assert qsuite(12) == 8

N = 50
for k in range(1, N):
    print(f"Q({k:2d}) = {qsuite(k)}")

